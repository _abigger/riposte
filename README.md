# Riposte

An Application for collecting RSVPs for an event.

## Requirements

- Ruby 2.2.0-p0

## Configuration

### Secrets

You'll need to add some secret tokens. To generate a secret use SecureRandom or:

```bash
bundle exec rake secret
```

#### Rails secrets

Ensure a secrets yaml file is created:

```bash
vi config/secrets.yml
```

Then ensure each environment has a secret_key_base key:

```YAML
development:
  secret_key_base: Your secret

test:
  secret_key_base: Yet another secret

# Do not keep production secrets in the repository,
# instead read values from the environment.
production:
  secret_key_base: <%= ENV["SECRET_KEY_BASE"] %>
```

### Devise Secret

Ensure devise config file (config/initializers/devise.rb) is set with a secret token:
`config.secret_key = 'Your devise secret'`

### Database

To create the database:

`bundle exec rake db:create`

Then run the migrations to set up the db and seed the database defaults:

```bash
bundle exec rake db:migrate
bundle exec rake db:seed
```

### Invitation Copy

All content on the site is kept in the English locale (config/locales/en.yml). Modifying this file will modify the content which appears on the front end.

It is organised by site section, with website globals kept up the top.

You may include html tags in the body text, however headings must be plain text only.

## Running the Test suite

`bundle exec rake` will run the entire test suite `bundle exec rspec spec` will run the application unit tests.

## Stock Photo Credits

Stock photos used are sourced from [Unsplash](https://unsplash.com/)

Photography by (in alphabetical order):

- [Melissa Askew](https://unsplash.com/teamaskew)
- [Kevin Curtis](https://unsplash.com/kcurtis113)
- [Anthony Delanoix](https://unsplash.com/anthonydelanoix)
- [Daniel Ebersole](https://unsplash.com/danielebersole)
- [Emanuel Feruzi](https://unsplash.com/k15photos)

## License

This project is covered by the [MIT licence](https://opensource.org/licenses/MIT), you can read it [here](https://bitbucket.org/biggerconcept/riposte/blob/master/LICENSE.md).
