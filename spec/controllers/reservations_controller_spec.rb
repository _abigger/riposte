require 'rails_helper'
RSpec.describe ReservationsController, type: :controller do
  let(:valid_attributes) {
    {
      email: 'some.one@lonelyplanet.com.au',
      guests_attributes: {
        '0' => {
          first_name: 'John',
          surname: 'Smith'
        }
      }
    }
  }

  let(:invalid_attributes) {
    {
      email: nil,
      guests_attributes: {
        '0'=> nil
      }
    }
  }

  let(:valid_session) { {} }

  describe "GET #new" do
    it "assigns a new reservation as @reservation" do
      get :new, {}, valid_session
      expect(assigns(:reservation)).to be_a_new(Reservation)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Reservation" do
        expect {
          post :create, {:reservation => valid_attributes}, valid_session
        }.to change(Reservation, :count).by(1)
      end

      it "assigns a newly created reservation as @reservation" do
        post :create, { reservation: valid_attributes }, valid_session
        expect(assigns(:reservation)).to be_a(Reservation)
        expect(assigns(:reservation)).to be_persisted
      end

      it "redirects to the confirm page" do
        post :create, { reservation: valid_attributes}, valid_session
        expect(response).to redirect_to(Reservation.last)
      end
    end

    context "with invalid params" do
      it "assigns a newly created but unsaved reservation as @reservation" do
        post :create, { reservation: invalid_attributes }, valid_session
        expect(assigns(:reservation)).to be_a_new(Reservation)
      end

      it "re-renders the 'new' template" do
        post :create, { reservation: invalid_attributes }, valid_session
        expect(response).to render_template("new")
      end
    end
  end
end
