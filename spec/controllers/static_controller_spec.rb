require 'rails_helper'

RSpec.describe StaticController, type: :controller do
  let(:valid_session) { {} }

  describe 'static pages' do
    it "renders index template" do
      post :index, valid_session
      expect(response).to render_template("index")
    end
  end
end
