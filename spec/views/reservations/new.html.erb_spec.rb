require 'rails_helper'

RSpec.describe "reservations/new", type: :view do
  before(:each) do
    assign(:reservation, Reservation.new(
      :email => "MyString"
    ))
  end

  it "renders new reservation form" do
    render
    assert_select "form[action=?][method=?]", reservations_path, "post" do
      assert_select "input#email[name=?]", "reservation[email]"
    end
  end
end
