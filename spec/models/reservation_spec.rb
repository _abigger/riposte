require 'rails_helper'

RSpec.describe Reservation, type: :model do
  describe 'email' do
    it { should validate_presence_of(:email) }
    it { should validate_uniqueness_of(:email) }
  end

  describe 'guests association' do
    it { should have_many(:guests) }
    it { should accept_nested_attributes_for(:guests) }
  end
end
