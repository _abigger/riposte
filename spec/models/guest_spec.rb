require 'rails_helper'

RSpec.describe Guest, type: :model do
  describe 'name' do
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:surname) }
  end

  describe 'staff' do
    it { should_not validate_presence_of(:staff) }
  end

  describe 'reservation association' do
    it { should belong_to(:reservation) }
  end
end
