When /^I follow the register button$/ do
  steps %Q{
    When I follow "#{I18n.t('invitation.signup.action')}"
  }
end

Then /^I should see the registration success message$/ do
  page.should have_content(I18n.t('confirmation.title'))
  page.should have_content(I18n.t('confirmation.message'))
end

Then /^I check the plus one prompt$/ do
  steps %Q{
    Then I check "#{I18n.t('registration.form.registered_guest.plus_one_label')}"
  }
end

Then /^I should see an error notice with:$/ do |error_details|
  page.should have_content("Your response was not saved")
  error_details.hashes.each do |details|
    page.should have_content(details['message'])
  end
end

