Feature: As party people, we should be able to rsvp
  Scenario: As known party people, we should be able to rsvp
    Given I am on the new reservation page
     When I fill in the following within "form.new_reservation":
        | email                             | jane.smith@example.com |
        | registered_guest-first-name       | Jane                   |
        | registered_guest-surname          | Smith                  |
     And I check the plus one prompt
    Then I fill in the following within "form.new_reservation":
        | guests-first-name                 | John                   |
        | guests-surname                    | Smith                  |
     And I press "Go!"
    Then I should see the registration success message

  Scenario: Responding to the invitation twice
    Given I am on the new reservation page
     When I fill in the following within "form.new_reservation":
        | email                             | jane.smith@example.com |
        | registered_guest-first-name       | Jane                   |
        | registered_guest-surname          | Smith                  |
     And I press "Go!"
    Then I go to the new reservation page
    Then I fill in the following within "form.new_reservation":
        | email                             | jane.smith@example.com |
        | registered_guest-first-name       | Jane                   |
        | registered_guest-surname          | Smith                  |
     And I press "Go!"
    Then I should see an error notice with:
       | Message               |
       | email: already taken  |

  Scenario: As anonymous party people, we should not be able to rsvp
    Given I am on the new reservation page
     When I check the plus one prompt
     Then I press "Go!"
     Then I should see an error notice with:
       | Message                         |
       | email: can't be blank           |
       | guest's name: can't be blank    |
       | guest's surname: can't be blank |
