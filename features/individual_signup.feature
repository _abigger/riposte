Feature: As a party person, I should be able to rsvp
  Scenario: Responding to the invitation
    Given I am on the homepage
     When I follow the register button
     Then I should be on the new reservation page

     When I fill in the following within "form.new_reservation":
        | email                             | jane.smith@example.com |
        | registered_guest-first-name       | Jane                   |
        | registered_guest-surname          | Smith                  |
     And I press "Go!"
    Then I should see the registration success message

  Scenario: Responding to the invitation twice
    Given I am on the new reservation page
     When I fill in the following within "form.new_reservation":
        | email                             | jane.smith@example.com |
        | registered_guest-first-name       | Jane                   |
        | registered_guest-surname          | Smith                  |
     And I press "Go!"
    Then I go to the new reservation page
    Then I fill in the following within "form.new_reservation":
        | email                             | jane.smith@example.com |
        | registered_guest-first-name       | Jane                   |
        | registered_guest-surname          | Smith                  |
     And I press "Go!"
    Then I should see an error notice with:
       | Message               |
       | email: already taken  |

  Scenario: Responding with out email or name
    Given I am on the new reservation page
     When I press "Go!"
     Then I should see an error notice with:
       | Message                         |
       | email: can't be blank           |
       | guest's name: can't be blank    |
       | guest's surname: can't be blank |
