Rails.application.routes.draw do
  root to: 'static#index'

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  resources :reservations
end
