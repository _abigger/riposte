class ReservationsController < ApplicationController
  before_action :set_reservation, only: [:show, :edit, :update, :destroy]

  # GET /reservations/new
  def new
    @reservation = Reservation.new
    @reservation.guests.build(staff: true)
    @reservation.guests.build(staff: false)
  end

  # GET /reservations/1/edit
  def edit
  end

  # POST /reservations
  def create
    @reservation = Reservation.new(reservation_params)
    unless @reservation.guests.any?
      # Build guest and plus one
      @reservation.guests.build(staff: true)
      @reservation.guests.build(staff: false)
    end

    respond_to do |format|
      if @reservation.save
        format.html do
          redirect_to @reservation,
          notice: 'Reservation was successfully created.'
        end
      else
        format.html { render :new }
      end
    end
  end

  private
    def set_reservation
      @reservation = Reservation.find(params[:id])
    end

    def reservation_params
      params.require(:reservation)
        .permit(
          :email,
          guests_attributes: [:first_name, :surname, :staff]
      )
    end
end
