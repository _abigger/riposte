class Reservation < ActiveRecord::Base
  validates :email, presence: true
  validates :email, uniqueness: true
  has_many :guests, dependent: :destroy
  accepts_nested_attributes_for(
    :guests,
    allow_destroy: true,
    reject_if: lambda { |g| g[:first_name].blank? }
  )
end
