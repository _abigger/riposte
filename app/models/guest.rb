class Guest < ActiveRecord::Base
  validates :first_name, presence: true
  validates :surname, presence: true
  belongs_to :reservation
end
