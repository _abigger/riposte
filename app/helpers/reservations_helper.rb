module ReservationsHelper
  def guest_labels(registered_guest=false)
    selector = registered_guest ? 'registered_guest' : 'guests'
    {
      form_label:             "registration.form.#{selector}.form_label",
      first_name:             "registration.form.#{selector}.first_name",
      first_name_placeholder: "registration.form.#{selector}.first_name_placeholder",
      surname:                "registration.form.#{selector}.surname",
      surname_placeholder:    "registration.form.#{selector}.surname_placeholder",
      id:                     "#{selector}"
    }
  end
end
