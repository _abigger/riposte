ActiveAdmin.register Guest do
  menu label: "Guest List"
  permit_params :first_name, :surname, :staff, :reservation_id

  # ACTIONS:
  actions :all, except: [:new]
  action_item :new do
    link_to 'New Reservation', new_admin_reservation_path
  end

  # FILTERS:
  begin
     preserve_default_filters!
  filter(:reservation, as: :select,
    collection: Reservation.all.map { |r| [r.email, r.id] },
    include_blank: true)
  rescue
  end

  # INDEX:
  index do
    selectable_column
    column :first_name
    column :surname
    column :staff
    column :created_at
    column "Reservation Made By", :reservation, sortable: :reservation
    actions
  end # index

  # FORM:
  form do |f|
    f.inputs "Guest Details" do
      f.input :first_name
      f.input :surname
      f.input :staff
      f.input(
        :reservation_id, as: :select,
        collection: Reservation.all.map { |r| [r.email, r.id] },
        include_blank: false
      )
    end # f.inputs
    f.actions
  end # form

  # SHOW:
  show do
    panel "Guest Details" do
      attributes_table_for guest do
        row("Id")                     { guest.id }
        row("First Name")             { guest.first_name }
        row("Surname")                { guest.surname }
        row("Staff") do
          guest.staff ? status_tag("yes", :ok) : status_tag("no")
        end # row
        row("Reservation")            { guest.reservation }
        row("Created At")             { guest.created_at }
        row("Updated At")             { guest.updated_at }
      end # attributes_table_for
    end # panel
  end # show
end #register Guest
