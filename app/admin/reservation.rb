ActiveAdmin.register Reservation do
  menu false
  permit_params :email,
    guests_attributes: [
      :id,
      :first_name,
      :surname,
      :staff,
      :requires_transport,
      :after_party_attendance,
      :_destroy
    ]

  # ACTIONS:
  action_item :new do
    link_to 'New Reservation', new_admin_reservation_path
  end

  # FILTERS:
  begin
    preserve_default_filters!
    filter(:guests, as: :select,
      collection: Guest.all.map { |r| ["#{r.surname}, #{r.first_name}", r.id] },
      include_blank: true)
  rescue
  end

  # INDEX:
  index do
    column :email
    column :guests, sortable: :guests do |reservation|
      reservation.guests.count
    end # column
    actions
  end # index

  # FORM
  form do |f|
    f.inputs "Reservation Details" do
      f.input :email
    end # f.inputs
    f.inputs 'Guests' do
      f.has_many :guests, heading: '', allow_destroy: true, new_record: true do |a|
        a.input :first_name
        a.input :surname
        a.input :staff
        a.actions
      end # has_many
    end # f.inputs
    f.actions do
      f.action(:submit) + f.cancel_link(admin_guests_path)
    end
  end # form

  # SHOW:
  show do
    panel "Reservation Details" do
      attributes_table_for reservation do
        row("Reservation Id") { reservation.id }
        row("Email") { reservation.email }
        row("Created At") { reservation.created_at }
      end # attributes_table_for reservation
    end # panel
    panel "Guest Details" do
      reservation.guests.each do |guest|
        attributes_table_for guest do
          row("#{title_of(guest)} Given Name:") { guest.first_name }
          row("#{title_of(guest)} Surname:") { guest.surname }
        end # attributes_table_for guest
      end # reservation.guests.each
    end # panel
  end # show
end # register Reservation

# HELPER:
def title_of(guest)
  guest.staff == true ? "Staff Member" : "Guest"
end
