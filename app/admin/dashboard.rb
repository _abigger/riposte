ActiveAdmin.register_page "Dashboard" do
  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    columns do
      column do
        panel "Guest Count" do
          guest_count = Guest.count
          h1 "#{guest_count}"
          h4 "guests are expected"
        end # panel
        panel "Plus One Count" do
          plus_one_count = Guest.where.not(staff: true).count
          h1 "#{plus_one_count}"
          h4 "guests are plus ones"
        end # panel
      end # column
      column do
        panel "Recently Updated Reservations" do
          ol do
            Reservation.order('updated_at desc').limit(20).each do |reservation|
              li link_to(reservation.email, admin_reservation_path(reservation))
            end # reservation
          end # ol
        end # panel
      end # column
    end # columns
  end # content
end # register Dashboard
