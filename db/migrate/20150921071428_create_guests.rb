class CreateGuests < ActiveRecord::Migration
  def change
    create_table :guests do |t|
      t.string :first_name
      t.string :surname
      t.boolean :staff
      t.belongs_to :reservation
      t.timestamps null: false
    end
  end
end
